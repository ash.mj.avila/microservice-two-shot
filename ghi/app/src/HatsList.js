import React from "react";

function HatList(props) {
  if (props.hats === undefined) {
    return null;
  }

  async function deleteHat(hat) {
    const deleteUrl = `http://localhost:8090/api/hats/${hat.id}/`;
    const deleteResponse = await fetch(deleteUrl, { method: "delete" });
    if (deleteResponse.ok) {
      console.log(`Deleted : ${hat.name}`);
      alert(`Deleted : ${hat.name}`);
    }
  }

  return (
    <table className="table">
      <thead className="thead-dark">
        <tr>
          <th>Image</th>
          <th>Name</th>
          <th>Style</th>
          <th>Fabric</th>
          <th>Color</th>
          <th>Location</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {props.hats.map((hat) => {
          return (
            <tr key={hat.id}>
              <td className="w-25">
                <img src={hat.image_url} className="img-thumbnail img" />
              </td>
              <td>{hat.name}</td>
              <td>{hat.style}</td>
              <td>{hat.fabric}</td>
              <td>{hat.color}</td>
              <td>{hat.location.closet_name}</td>
              <td>
                <button
                  className="btn btn-secondary btn-xs"
                  onClick={() => deleteHat(hat)}
                >
                  Remove
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
export default HatList;
