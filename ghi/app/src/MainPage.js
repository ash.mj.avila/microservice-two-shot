import React from "react";
import Carousel from "react-bootstrap/Carousel";
import { Link } from "react-router-dom";

function MainPage() {
  return (
    <>
      <div className="px-4 py-5 my-5 mt-0 text-center bg-light">
        <div className='row justify-content-center'>
          <Carousel interval="3000">
            <Carousel.Item>
              <img
                height={500}
                className="d-block mx-auto"
                src="https://tinyurl.com/4h446zp4"
                alt="First slide"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                height={500}
                className="d-block mx-auto"
                src="https://tinyurl.com/2s3w3dkw"
                alt="Second slide"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                height={500}
                className="d-block mx-auto"
                src="https://tinyurl.com/ysru8u79"
                alt="Third slide"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                height={500}
                className="d-block mx-auto"
                src="https://tinyurl.com/bdf7bxw2"
                alt="Third slide"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                height={500}
                className="d-block mx-auto"
                src="https://tinyurl.com/44tt4b6y"
                alt="Third slide"
              />
            </Carousel.Item>
          </Carousel>
          <h1 className="mt-4 display-5 fw-bold">WARDROBIFY!</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-2">
              Need to keep track of your shoes and hats?
            </p>
            <p className="lead mb-3">We have the solution for you!</p>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center lead mb-3">
              <Link to="/hats/new" className="btn btn-secondary btn-lg px-4 gap-3">Make New Hat</Link>
            </div>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
              <Link to="/shoes/new" className="btn btn-secondary btn-lg px-4 gap-3">Make New Shoes</Link>
            </div>
          </div>
        </div>
      </div>
    </>

  );
};


export default MainPage;
