import React, { useEffect, useState } from 'react';

function HatsForm() {

    const [locations, setLocations] = useState([]);

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value)
    }

    const [style, setStyle] = useState('');
    const handleStyleChange = (event) => {
        const value = event.target.value;
        setStyle(value)
    }

    const [fabric, setFabric] = useState('');
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value)
    }

    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value)
    }

    const [image_url, setImageURL] = useState('');
    const handleImageURLChange = (event) => {
        const value = event.target.value;
        setImageURL(value)
    }

    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.style = style;
        data.fabric = fabric;
        data.color = color;
        data.image_url = image_url;
        data.location = location;

        console.log(data);

        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const response = await fetch(hatsUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setName('');
            setStyle('');
            setFabric('');
            setColor('');
            setImageURL('');
            setLocation('');
        }
    }
    const fetchData = async () => {
        const locationUrl = 'http://localhost:8100/api/locations/';
        const locationResponse = await fetch(locationUrl);

        if (locationResponse.ok) {
            const data = await locationResponse.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a New Hat</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name"
                                id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStyleChange} value={style} placeholder="Style" required type="text" name="style"
                                id="style" className="form-control" />
                            <label htmlFor="style">Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFabricChange} value={fabric} placeholder="Fabric" type="text" name="fabric" id="fabric"
                                className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color"
                                className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="image_url" className="form-label">Image URL</label>
                            <textarea onChange={handleImageURLChange} value={image_url} className="form-control" name="image_url" id="image_url" rows="3"></textarea>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} value={location} required id="location" name="location" className="form-select">
                                <option value="">Choose a wardrobe location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.href} value={location.href}>
                                            {location.closet_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create Hat</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default HatsForm;
