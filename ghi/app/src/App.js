import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import HatsForm from "./HatsForm";
import HatsList from "./HatsList";
import ShoeList from "./ShoeList";
import ShoeForm from "./ShoeForm";
import Nav from "./Nav";

function App(props) {
  if (props.shoes === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="shoes">
            <Route index element={<ShoeList shoes={props.shoes} />} />
            <Route path="new" element={<ShoeForm />} />
          </Route>
          <Route path="/">
            <Route path="" element={<MainPage />} />
          </Route>
          <Route path="/hats">
            <Route path="" element={<HatsList hats={props.hats} />} />
            <Route path="new" element={<HatsForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
