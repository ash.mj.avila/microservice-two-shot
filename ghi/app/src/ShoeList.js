import React from "react";

function ShoeList(props) {
  if (props.shoes === undefined) {
    return null;
  }
  async function deleteShoe(shoe) {
    const deleteUrl = `http://localhost:8080/api/shoes/${shoe.id}/`;
    console.log(deleteUrl);
    const deleteResponse = await fetch(deleteUrl, { method: "delete" });
    if (deleteResponse.ok) {
      console.log(`Deleted : ${shoe.model_name}`);
      alert(`Deleted : ${shoe.model_name}`);
    }
  }
  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Model Name</th>
            <th>color</th>
            <th>image</th>
            <th>Bin</th>
          </tr>
        </thead>
        <tbody>
          {props.shoes.map((shoe) => {
            return (
              <tr key={shoe.id}>
                <td>{shoe.name}</td>
                <td>{shoe.manufacturer}</td>
                <td>{shoe.model_name}</td>
                <td>{shoe.color}</td>
                <td className="w-25">
                  <img src={shoe.picture_url} className="img-thumbnail img" />
                </td>
                <td>{shoe.bin.closet_name}</td>
                <td>
                  <button
                    className="btn btn-seconaday btn-xs"
                    onClick={() => deleteShoe(shoe)}
                  >
                    Remove
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ShoeList;
