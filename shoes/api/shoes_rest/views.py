from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from django.shortcuts import render
from .models import Shoe, BinVO



class BinVODetailEncdoer(ModelEncoder):
    model = BinVO
    properties = [
        'import_href',
        'closet_name',
        'bin_number',
        'bin_size'
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        'manufacturer',
        'model_name',
        'color',
        'bin',
        'id',
        'picture_url'
        'name',
        ]
    encoders = {
        "bin": BinVODetailEncdoer(),
    }

class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        'manufacturer',
        'model_name',
        'color',
        'picture_url',
        'bin',
        'name',
        ]
    encoders = {
        "bin": BinVODetailEncdoer(),
    }


# GET LIST OF SHOES
@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )

    else:
        content = json.loads(request.body)

        # try:
        #     # some code here
        # except Shoe.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "Invalid Shoe id"},
        #         status=400,
        #     )


# get bin object
        bin_href = content["bin"]
        bin = BinVO.objects.get(import_href=bin_href)
        content["bin"] = bin

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )




# EXAMPLE OF A VIEW
# def api_list_conferences(request):
    response = []
    conferences = Conference.objects.all()
    for conference in conferences:
        response.append(
            {
                "name": conference.name,
                "href": conference.get_api_url(),
            }
        )
    return JsonResponse({"conferences": response})

# SHOW A PAIR OF SHOES
@require_http_methods(["GET","DELETE"])
def api_show_shoe(request, id):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": True})
