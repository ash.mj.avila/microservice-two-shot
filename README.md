# Wardrobify

Team:

- David Jimenez - Shoes
- Ash Avila - Hats

## Design

We will be working on the frontend, merging when finished. Then we will complete backend and merge and resolve all conflicts.

## Shoes microservice

Backend Operations
-Build a Model for the Shoe class with a foreign key connected to a bin that shows the size of the bin and location relative to the wardrobe.
-Register the Shoe App to wardrobe_project settings
-Set up the models in our views.py to process request and generating
response ("GET","POST","DELETE")
-Register the views in the application urls and then to the project urls
-SetUp a script that polls a REST API for data on shoe storage bins and updates and creates corresponding resords in a databse
-SetUp RESTful API's using insomnia to manipulate data using HTTP methods for the Shoe and Bin models

FrontEnd Operations

- Made javascript files incorporating JSX syntax to create a List of shoes and a form to create a shoe and add it to an existing bin to place in a wardrobe
- Create hooks to hook (lol) and handke callsfrom UI to database List
  -Make page readable, able to create a shoe, and shoe list of shoes

## Hats microservice
Hats approach:
	Backend:
        Build Insomnia for existing service, test it
            Generate project > name it > start new collection > add services (“HTTP” + service type + JSON body)
        Build models, according to specs
            Add all expected elements with their corresponding data types > generate a VO to interact with the poller on the information you want to receive from the location model
        Build poller to interact with the frontend
            Make sure its polling the correct database (for the wardrobe location) > ensure the poller matches the VO and the VO matches the input
        Build views for to integrate front and backend data and services
            Generate views for show and list items > show will include a GET & DELETE method > list will provide GET and POST method
        Add to insomnia accordingly using the correct host and the urls that correspond with the desired data
	Frontend:
        Make .js files for Form and List
            Form: include hooks to handle the calls from the ui to the database
            List: Link existing database to a format thats easy to read, including a way to delete items based on their id/href
        Write the functions to connect the backend to the ui
            Update the index, app, & nav files to link all of the data
                Index: renders a root for the data
                App: Sets route path’s for each .js file/element
                Nav: generates links in the nav bar to easily move through the ui
