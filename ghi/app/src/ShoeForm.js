import React, { useEffect, useState } from "react";

function ShoeForm() {
  const [bins, setBins] = useState([]);
  const [bin, setBin] = useState("");
  const [manufacturer, setManufacturer] = useState("");
  const [modelName, setModelName] = useState("");
  const [name, setName] = useState("");
  const [color, setColor] = useState("");
  const [pictureUrl, setPictureUrl] = useState("");

  const handelBinChange = (event) => {
    const value = event.target.value;
    setBin(value);
  };

  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleModleNameChange = (event) => {
    const value = event.target.value;
    setModelName(value);
  };

  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  };

  const handlePictureUrlChange = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};

    data.manufacturer = manufacturer;
    data.model_name = modelName;
    data.name = name;
    data.color = color;
    data.picture_url = pictureUrl;
    data.bin = bin;

    const shoeUrl = "http://localhost:8080/api/shoes/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "content-Type": "application/json",
      },
    };

    const response = await fetch(shoeUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe);

      setBin("");
      setModelName("");
      setName("");
      setManufacturer("");
      setColor("");
      setPictureUrl("");
    }
  };

  const fetchData = async () => {
    const binUrl = "http://localhost:8100/api/bins/";
    const response = await fetch(binUrl);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Shoe</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleManufacturerChange}
                value={manufacturer}
                placeholder="Manufacturer"
                required
                type="text"
                name="manufacturer"
                id="manufacturer"
                className="form-control"
              />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>

            <div className="form-floating mb-3">
              <input
                placeholder="Model Name"
                required
                type="text"
                name="model_name"
                id="model_name"
                className="form-control"
                value={modelName}
                onChange={handleModleNameChange}
              />
              <label htmlFor="model_name">Model Name</label>
            </div>

            <div className="form-floating mb-3">
              <input
                placeholder="Name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
                value={name}
                onChange={handleNameChange}
              />
              <label htmlFor="name">Name</label>
            </div>

            <div className="form-floating mb-3">
              <input
                onChange={handleColorChange}
                value={color}
                placeholder="Color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>

            <div className="form-floating mb-3">
              <input
                placeholder="Picture URL"
                required
                type="text"
                name="picture_url"
                id="picture_url"
                className="form-control"
                value={pictureUrl}
                onChange={handlePictureUrlChange}
              />
              <label htmlFor="picture_url">PictureUrl</label>
            </div>

            <div className="mb-3">
              <select
                value={bin}
                onChange={handelBinChange}
                required
                name="bin"
                id="bin"
                className="form-select"
              >
                <option value="">Choose a bin</option>
                {bins.map((bin) => {
                  return (
                    <option key={bin.href} value={bin.href}>
                      {bin.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;
